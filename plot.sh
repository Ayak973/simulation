#!/bin/bash

gnuplot --persist -e " \
set xlabel 'x/10e-6';set ylabel 'y/10e-6'; \
set object circle at first 0,0 size scr 0.0025 fc rgb '#ed3504' fill solid noborder; \
plot '`pwd`/build/data.txt' using 1:2 with lines lt rgb '#448769' lw 1  dt 1 title 'earth', '`pwd`/build/data.txt' using 3:4 with lines lt rgb '#0b5022' lw 1  dt 1 title 'mars', '`pwd`/build/data.txt' using 5:6 with lines lt rgb '#e368d3' lw 1  dt 1 title 'mercury', '`pwd`/build/data.txt' using 7:8 with lines lt rgb '#6fa684' lw 1  dt 1 title 'venus', '`pwd`/build/data.txt' using 9:10 with lines lt rgb '#951e69' lw 1  dt 1 title 'sun', ;pause -1;"
