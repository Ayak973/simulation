# Output file name
DISTNAME           	= simulation

# Output directory
DISTDIR				= build

# Includes dir
INCDIRS            	= 

# Sources dir
SRCDIR				= src

# Library dir and modules
LIBDIR             	= 
LIBS				= -lpthread

# Tools path
CXX                	= @g++
COPY_EXEC          	= @cp -rf
MOVE_EXEC          	= @mv -f
MKDIR_EXEC         	= mkdir -p
TEST_EXEC          	= @test -d
ECHO_EXEC          	= @echo
RM_EXEC            	= @rm -rf
DELETEOBJS			= find . -type f -name '*.o' -exec rm {} +

# C++ flags
CXXFLAGS           	= -c -Wall -std=c++17 -Werror

# Souces files (copy all *.cpp files in src dir, not following subdirs)
SOURCES           	= $(shell find $(SRCDIR)/ -name '*.cpp')

# Normal headers
HEADERS				= $(notdir $(shell find . -maxdepth 1 -name '*.h'))

# Objects files
OBJS				= $(SOURCES:%.cpp=%.o)

release: CXXFLAGS     	+= -O3
release: releaseinfo createdir $(DISTNAME) cleanbuild
	$(ECHO_EXEC) Release done !

# Add DEBUG define and debug infos for gdb (-ggdb)
debug: CXXFLAGS += -D_DEBUG 
debug: CXXFLAGS += -ggdb
debug: debuginfo createdir $(DISTNAME) cleanbuild
	$(ECHO_EXEC) Debug done !

# Executable rule
$(DISTNAME): $(OBJS)
	$(ECHO_EXEC) Building $(DISTNAME)
	$(CXX) $^ $(LIBS) $(LIBDIR) -o $(DISTDIR)/$@

# Generate object files
$(OBJS): %.o : %.cpp
	$(CXX) $(CXXFLAGS) $(INCDIRS) -c $< -o $@

cleanbuild:
	@echo Cleaning obj dir
	@$(DELETEOBJS)

debuginfo:
	$(ECHO_EXEC) Starting Debug...

releaseinfo:
	$(ECHO_EXEC) Starting Release

createdir:
	@$(MKDIR_EXEC) $(DISTDIR)

.PHONY: clean
clean: cleanbuild
	$(RM_EXEC) -rf $(DISTDIR)
	$(ECHO_EXEC) Clean done !