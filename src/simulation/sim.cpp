#include "sim.h"


#include <cmath>


/////////////////////BODY

sim::Body::Body(std::string const& name, glm::dvec2 const& pos, glm::dvec2 const& vel, double mass) : nameStr(name), m(mass), rad(1.0), 
        accel(0.0), vel(vel), pos(pos) { }


glm::dvec2 const& sim::Body::position() const {
    return pos;
}

double const& sim::Body::mass() const {
    return m;
}

std::string const& sim::Body::name() const {
    return nameStr;
}

double sim::Body::distance(Body const& externalBody) const {
    return std::sqrt(std::pow(externalBody.position().x - this->pos.x, 2) + std::pow(externalBody.position().y - this->pos.y, 2));
}

void sim::Body::update(double dt) {
    //we use explicit?? euler integration => if not enough accuracy, try a RK4 algorithm
    vel.x += ((accel.x / m) * dt);
    vel.y += ((accel.y / m) * dt);

    pos.x += (vel.x * dt);
    pos.y += (vel.y * dt);
}


/////////////////////NBODY

sim::NBody::NBody() {  }

sim::NBody::~NBody() {  }

void sim::NBody::add(std::string const& name, glm::dvec2 const& pos, glm::dvec2 const& vel, double mass) { 
    bodyVector.emplace_back(name, pos, vel, mass);
}

void sim::NBody::update(double elapsed) {  
    for (auto& body : bodyVector) {
        body.gravityAccel(bodyVector);
    }

    for (auto& body : bodyVector) {
        body.update(elapsed);
    }
}

std::vector<sim::Body> const& sim::NBody::bodies() const {
    return bodyVector;
}

/////////////////////SOLARNBODY

sim::SolarNBody::SolarNBody() {
    add("earth", glm::dvec2(1.4960e11, 0.0), glm::dvec2(0.0, 2.9800e4), 5.9740e24);
    add("mars", glm::dvec2(2.2790e11, 0.0), glm::dvec2(0.0, 2.4100e4), 6.4190e23);
    add("mercury", glm::dvec2(5.7900e10, 0.0), glm::dvec2(0.0, 4.7900e4), 3.3020e23);
    add("venus", glm::dvec2(1.0820e11, 0.0), glm::dvec2(0.0, 3.500e4), 4.8690e24);
    add("sun", glm::dvec2(0.0, 0.0), glm::dvec2(0.0, 0.0), 1.9890e30);
}


/////////////////////RK4

sim::Derivative sim::RK4::evaluate(State const& targetBody, State const& externalBody, float dt, Derivative const& d) {
    State tmpState;
    //tmpState.mass = targetBody.mass;
    tmpState.position.x = (targetBody.position.x) + d.dx.x * dt;
    tmpState.position.y = (targetBody.position.y) + d.dx.y * dt;

    tmpState.velocity.x = (targetBody.velocity.x) - d.dv.x * dt;
    tmpState.velocity.y = (targetBody.velocity.y) - d.dv.y * dt;

    Derivative output;
    output.dx.x = tmpState.velocity.x;
    output.dx.y = tmpState.velocity.y;

    output.dv = gravityAccel(tmpState, externalBody);
    return output;
}

glm::vec2 sim::RK4::gravityAccel(State const& targetBody, State const& externalBody) {
    double r = std::sqrt(std::pow(externalBody.position.x - targetBody.position.x, 2) + std::pow(externalBody.position.y - targetBody.position.y, 2)); //glm::distance(state.position, glm::vec2(0.0,0.0));
    //std::cout << "r: " << r <<  " - " << (6.67408e-11 * externalBody.mass) << std::endl;
    double g = (6.67408e-11 * externalBody.mass) / std::pow(r, 2);
    double theta = std::atan2(targetBody.position.x - externalBody.position.x, targetBody.position.y - externalBody.position.y);
    //glm::vec2 rNorm = glm::normalize(targetBody.position);
    //std::cout << "g: " << g << " - r: " << r << " - rNorm: " << rNorm.x << ":" << rNorm.y << std::endl;
    //if (std::isnan(g) || std::isinf(g) || std::isnan(rNorm.x) || std::isnan(rNorm.y)) return glm::vec2(0.f,0.f);
    return glm::vec2(g * std::sin(theta), g * std::cos(theta));
}

//https://github.com/pauljxtan/nbody/blob/master/src/nbody_rk4.cpp
//http://codeflow.org/entries/2010/aug/28/integration-by-example-euler-vs-verlet-vs-runge-kutta/

//equations: https://www.ajdesigner.com/phpgravity/gravity_acceleration_equation.php

void sim::RK4::integrate(State & targetBody, State const& externalBody, float dt) {
    Derivative a, b, c, d;
    
    a = evaluate(targetBody, externalBody, 0.f, Derivative());
    b = evaluate(targetBody, externalBody, dt * 0.5f, a);
    c = evaluate(targetBody, externalBody, dt * 0.5f, b);
    d = evaluate(targetBody, externalBody, dt, c);

    float dxdtx = 1.f / 6.f * (a.dx.x + 2.f * (b.dx.x + c.dx.x) + d.dx.x);
    float dxdty = 1.f / 6.f * (a.dx.y + 2.f * (b.dx.y + c.dx.y) + d.dx.y);

    float dvdtx = 1.f / 6.f * (a.dv.x + 2.f * (b.dv.x + c.dv.x) + d.dv.x);
    float dvdty = 1.f / 6.f * (a.dv.y + 2.f * (b.dv.y + c.dv.y) + d.dv.y);

    targetBody.position.x = targetBody.position.x + dxdtx * dt;
    targetBody.position.y = targetBody.position.y + dxdty * dt;

    targetBody.velocity.x = targetBody.velocity.x - dvdtx * dt;
    targetBody.velocity.y = targetBody.velocity.y - dvdty * dt;
}

sim::Derivative sim::RK4::deriv(State const& targetBody, State const& externalBody, float dt) {
    Derivative a, b, c, d;

    a = evaluate(targetBody, externalBody, 0.f, Derivative());
    b = evaluate(targetBody, externalBody, dt * 0.5f, a);
    c = evaluate(targetBody, externalBody, dt * 0.5f, b);
    d = evaluate(targetBody, externalBody, dt, c);

    glm::vec2 dxdt(1.f / 6.f * (a.dx.x + 2.f * (b.dx.x + c.dx.x) + d.dx.x), 1.f / 6.f * (a.dx.y + 2.f * (b.dx.y + c.dx.y) + d.dx.y));
    glm::vec2 dvdt(1.f / 6.f * (a.dv.x + 2.f * (b.dv.x + c.dv.x) + d.dv.x), 1.f / 6.f * (a.dv.y + 2.f * (b.dv.y + c.dv.y) + d.dv.y));

    return Derivative(dxdt, dvdt);
}

void sim::ExplicitEuler::advance(State & targetBody, State const& externalBody, float dt) {
    //dt *= 10.0;
    targetBody.position.x += targetBody.velocity.x * dt;
    targetBody.position.y += targetBody.velocity.y * dt;

    glm::vec2 g = gravityAccel(targetBody, externalBody);
    targetBody.velocity.x = targetBody.velocity.x - g.x * dt;
    targetBody.velocity.y = targetBody.velocity.y - g.y * dt;
}

glm::vec2 sim::ExplicitEuler::gravityAccel(State const& targetBody, State const& externalBody) {
    //GravityConstant * (mass / std::pow(radius, 2));
    //float angle = std::atan2(state.position.x, state.position.y);
    double r = std::sqrt(std::pow(externalBody.position.x - targetBody.position.x, 2) + std::pow(externalBody.position.y - targetBody.position.y, 2)); //glm::distance(state.position, glm::vec2(0.0,0.0));
    double g = 6.67408e-11 * (externalBody.mass / std::pow(r, 2));
    glm::vec2 rNorm = glm::normalize(targetBody.position);
    return glm::vec2(g * rNorm.x,g * rNorm.y);
}


void sim::SemiImplicitEuler::advance(State &targetBody, State const& externalBody, float dt) {
    glm::vec2 g = gravityAccel(targetBody, externalBody);
    targetBody.velocity.x = targetBody.velocity.x - g.x * dt;
    targetBody.velocity.y = targetBody.velocity.y - g.y * dt;

    targetBody.position.x += targetBody.velocity.x * dt;
    targetBody.position.y += targetBody.velocity.y * dt;
}

glm::vec2 sim::SemiImplicitEuler::gravityAccel(State const& targetBody, const State &externalBody) {
    //GravityConstant * (mass / std::pow(radius, 2));
    //float angle = std::atan2(state.position.x, state.position.y);
    double r = glm::distance(externalBody.position, targetBody.position); //glm::distance(state.position, glm::vec2(0.0,0.0));
    double g = 6.67408e-11 * (externalBody.mass / std::pow(r, 2));
    glm::vec2 rNorm = glm::normalize(targetBody.position);
    return glm::vec2(g * rNorm.x,g * rNorm.y);
}


glm::vec2 sim::RungeKutta4::partialStep(glm::vec2 p1, glm::vec2 p2, float dt) {
    return glm::vec2(p1.x + p2.x * dt, p1.y + p2.y * dt);
}

void sim::RungeKutta4::computeVelocity(State &objState, State const& externalBody, float dt) {
    glm::vec2 acceleration = singleObjectAccel(objState, externalBody, dt);
    objState.velocity.x += acceleration.x * dt;
    objState.velocity.y += acceleration.y * dt;
}

void sim::RungeKutta4::updateLocation(State &objState, float dt) {
    objState.position.x += objState.velocity.x * dt;
    objState.position.y += objState.velocity.y * dt;
}

glm::vec2 sim::RungeKutta4::singleObjectAccel(const State &targetBody, const State &externalBody, float dt) {
    double G = 6.67408e-11f;
    glm::vec2 accel(0.f,0.f);
    double radius = std::sqrt(std::pow(externalBody.position.x - targetBody.position.x, 2) + std::pow(externalBody.position.y - targetBody.position.y, 2));
    double g = G * externalBody.mass / (radius * radius * radius);

    glm::vec2 k1(0.f,0.f);
    glm::vec2 k2(0.f,0.f);
    glm::vec2 k3(0.f,0.f);
    glm::vec2 k4(0.f,0.f);

    State tmpState;

    k1.x = g * (externalBody.position.x - targetBody.position.x);
    k1.y = g * (externalBody.position.y - targetBody.position.y);

    tmpState.velocity = partialStep(targetBody.velocity, k1, 0.5);
    tmpState.position = partialStep(targetBody.position, tmpState.velocity, 0.5);

    k2.x = (externalBody.position.x - tmpState.position.x) * g;
    k2.y = (externalBody.position.y - tmpState.position.y) * g;

    tmpState.velocity = partialStep(targetBody.velocity, k2, 0.5);
    tmpState.position = partialStep(targetBody.position, tmpState.velocity, 0.5);

    k3.x = (externalBody.position.x - tmpState.position.x) * g;
    k3.y = (externalBody.position.y - tmpState.position.y) * g;

    tmpState.velocity = partialStep(targetBody.velocity, k3, 1.0);
    tmpState.position = partialStep(targetBody.position, tmpState.velocity, 1.0);

    k4.x = (externalBody.position.x - tmpState.position.x) * g;
    k4.y = (externalBody.position.y - tmpState.position.y) * g;

    accel.x += (k1.x + k2.x * 2.0 + k3.x * 2.0 + k4.x) / 6.0;
    accel.y += (k1.y + k2.y * 2.0 + k3.y * 2.0 + k4.y) / 6.0;

    return accel;
}

void sim::RungeKutta4::integrate(State &targetBody, const State &externalBody, float dt) {
    //State externalBody(glm::vec2(0.f, 0.f), glm::vec2(0.f, 0.f));
    //dt *= 10.0;
    computeVelocity(targetBody, externalBody, dt);
    updateLocation(targetBody, dt);
}



