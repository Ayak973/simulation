#ifndef SIM_H
#define SIM_H

#include <vector>
#include <string>

#include <glm/glm.hpp>

//https://www.youtube.com/watch?v=G5zxfMmRtsk

namespace sim {
    class Body {
        private:
            std::string nameStr;

            double m;
            double rad;

            glm::dvec2 accel;   //acceleration over time
            glm::dvec2 vel;     //derivative of accel over time
            glm::dvec2 pos;     //derivative of vel over time

            double distance(Body const& externalBody) const;

        public:
            Body() = delete;
            Body(std::string const& name, glm::dvec2 const& pos, glm::dvec2 const& vel, double mass);

            glm::dvec2 const& position() const;
            double const& mass() const;
            std::string const& name() const;
            double const& radius() const;

            void update(double dt);

            template<template<typename, typename> class Container>
            void gravityAccel(Container<Body, std::allocator<Body>> const& bodies) {
                accel = glm::dvec2(0.0);
                
                for (auto& externalBody : bodies) {
                    if (&externalBody == this) continue;
                    double distance = this->distance(externalBody);
                    double g = (6.67e-11 * externalBody.mass()) * this->m / std::pow(distance, 2);
                    accel.x += g * (externalBody.position().x - pos.x) / distance;
                    accel.y += g * (externalBody.position().y - pos.y) / distance;
                }
            }
    };

    class NBody {
        private:
            std::vector<Body> bodyVector;

        protected:
            NBody();

            void add(std::string const& name, glm::dvec2 const& pos, glm::dvec2 const& vel, double mass);
        
        public:
            virtual ~NBody();
            
            void update(double elapsed);

            std::vector<Body> const& bodies() const;
    };

    class SolarNBody final : public NBody {
        public:
            SolarNBody();
    };

    struct State {
        std::string name;
        bool moving;

        glm::vec2 position;
        glm::vec2 velocity;

        float mass;

        State(std::string name, glm::vec2 const& pos, glm::vec2 const& velocity, float mass, bool moving = true) : name(name), moving(moving), position(pos), velocity(velocity), mass(mass) {  }
        State() : name("tmp"), moving(true), position(glm::vec2(0.f, 0.f)), velocity(glm::vec2(0.f, 0.f)), mass(1.f) {  }
    };

    struct Derivative {
        glm::vec2 dx;   // dx/dt = velocity
        glm::vec2 dv;   // dv/dt = acceleration

        Derivative(glm::vec2 velocity, glm::vec2 acceleration) : dx(velocity), dv(acceleration) {  }
        Derivative() : dx(glm::vec2(0.f,0.f)), dv(glm::vec2(0.f, 0.f)) {  }

        void operator+=(Derivative const& deriv) {
            dx += deriv.dx;
            dv += deriv.dv;
        }

        void operator-=(Derivative const& deriv) {
            dx -= deriv.dx;
            dv -= deriv.dv;
        }
    };

    class RK4 {
        private:
            static Derivative evaluate(State const& targetBody, State const& externalBody, float dt, Derivative const& d);
            static glm::vec2 gravityAccel(State const& targetBody, State const&externalBody);

            RK4() = delete;

        public:
            static void integrate(State &targetBody, State const& externalBody, float dt);
            static Derivative deriv(State const& targetBody, State const& externalBody, float dt);
    };

    class RungeKutta4 {
        private:
            static glm::vec2 partialStep(glm::vec2 p1, glm::vec2 p2, float dt);

            static void computeVelocity(State & objState, const State &externalBody, float dt);
            static void updateLocation(State & objState, float dt);
            static glm::vec2 singleObjectAccel(State const& targetBody, State const& externalBody, float dt);

        public:
            RungeKutta4() = delete;

            static void integrate(State &targetBody, State const& externalBody, float dt);
    };

    class ExplicitEuler {
        private:

        public:
            ExplicitEuler() = delete;

            static void advance(State &targetBody, State const& externalBody, float dt);

            static glm::vec2 gravityAccel(State const& targetBody, const State &externalBody);
    };

    class SemiImplicitEuler {
        private:
            static glm::vec2 gravityAccel(State const& targetBody, State const&externalBody);

        public:
            SemiImplicitEuler() = delete;

            static void advance(State &objState, const State &externalBody, float dt);
    };
}

#endif // SIM_H
