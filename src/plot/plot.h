#ifndef PLOT_H
#define PLOT_H

#include "../simulation/sim.h"

#include <fstream>
#include <string>

namespace plot {

    class NBodyPlot {
        private:
            std::ofstream dataStreamOutput;
            std::ofstream dataStreamPlot;

            sim::NBody const& bodiesRef;

        public:
            NBodyPlot() = delete;
            NBodyPlot(std::string const& dataFile, std::string const& plotScript, sim::NBody const& bodiesRef);
            ~NBodyPlot();

            void writeData();
            void writeScript();
    };
}

#endif //PLOT_H