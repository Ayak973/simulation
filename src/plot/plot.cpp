#include "plot.h"

#include <sstream>
#include <random>
#include <iomanip>

plot::NBodyPlot::NBodyPlot(std::string const& dataFile, std::string const& plotScript, sim::NBody const& bodiesRef) : dataStreamOutput(dataFile, std::ios::trunc), dataStreamPlot(plotScript, std::ios::trunc), bodiesRef(bodiesRef) {
    if (!dataStreamOutput.is_open()) throw std::runtime_error("cannot open " + dataFile);
    if (!dataStreamPlot.is_open()) throw std::runtime_error("cannot open " + plotScript);
}

plot::NBodyPlot::~NBodyPlot() {
    dataStreamOutput.close();
    dataStreamPlot.close();
}

void plot::NBodyPlot::writeData() {
    if (bodiesRef.bodies().size() == 0) return;
    for (auto & body : bodiesRef.bodies()) {
        dataStreamOutput << body.position().x << " " << body.position().y << " ";
    }
    
    dataStreamOutput << std::endl;
}

void plot::NBodyPlot::writeScript() {
    if (bodiesRef.bodies().size() == 0) throw std::runtime_error("cannot write plot script: no bodies");

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(0x00, 0xFF);

    dataStreamPlot << R"EOD(#!/bin/bash

gnuplot --persist -e " \
set xlabel 'x/10e-6';set ylabel 'y/10e-6'; \
set object circle at first 0,0 size scr 0.0025 fc rgb '#ed3504' fill solid noborder; \
plot )EOD";
    std::size_t coordIdx = 0;
    for (auto & body : bodiesRef.bodies()) {
        std::stringstream color;
        color << std::hex << std::setfill('0') << std::setw(2) << dist(mt) << dist(mt) << dist(mt);
        dataStreamPlot << "'`pwd`/build/data.txt' using " << (++coordIdx) << ":";
        dataStreamPlot << (++coordIdx) << " with lines lt rgb '#";
        dataStreamPlot << color.str() << "' lw 1 dt 1 title '" << body.name() << "', ";
    }

    dataStreamPlot << ";pause -1;\"" << std::endl;
}