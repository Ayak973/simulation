#include <iostream>

#include <fstream>

#include <chrono>
#include <thread>
#include <limits>

#include "simulation/sim.h"
#include "plot/plot.h"

static const std::string GreenColor = "\033[1m\033[32m";
static const std::string ResetConsole = "\033[0m";
static const std::string BoldCyanColor = "\033[1m\033[36m";
static const std::string RedColor = "\033[1m\033[31m";
static const std::string WhatColor = "\033[1m\033[33m";

static constexpr float scale = 1.0f;

void printUsage();

int main(int argc, char* argv[]) {
    //arguments check
    if (argc != 2) {
		printUsage();
		return EXIT_FAILURE;
    }
    //

    //solar system init
    sim::SolarNBody solarNBody;

    //output file for data plotting 
    std::string filePath(argv[1]);
    plot::NBodyPlot plot(filePath, "plot.sh", solarNBody);
    //write initial solar system state
    plot.writeData();

    //timing
    static const int timestep = 1;                                      //simulation timestep in sec
    float timestepMs = timestep;                                        //real timestep
    static const uint64_t simDuration = 365 * 24 * 60 * 60 * timestep;  //simulation total duration

    uint64_t simElapsed = 0;
    int steps = 0;

    double loopTotalTime = 0.0;
    double loopMinTime = std::numeric_limits<double>::max();
    double loopMaxTime = 0.0;
    double stepTime = 0.0;
    std::chrono::high_resolution_clock::time_point stepStart;
    //

    std::cout << ResetConsole << "Starting simu with " << GreenColor << solarNBody.bodies().size() << ResetConsole << " bodies - timestep = " << GreenColor << timestep << "s" << ResetConsole << " - sim duration: " << GreenColor << (simDuration) << "s" << ResetConsole << std::endl;
    std::cout << ResetConsole << "\tOutput written in file " << GreenColor << filePath << ResetConsole << std::endl;

    //total chrono
    auto start = std::chrono::high_resolution_clock::now();
    //

    while (simElapsed < simDuration) {
        stepStart = std::chrono::high_resolution_clock::now();

        //update solar system state
        solarNBody.update(timestepMs);

        stepTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - stepStart).count();
        loopTotalTime += stepTime;
        if (stepTime > loopMaxTime) loopMaxTime = stepTime;
        if (stepTime < loopMinTime) loopMinTime = stepTime;

        //write to plot file (each 1000 times)
        if ((steps % 1000) == 0) plot.writeData();

        ++steps;
        simElapsed += timestep;
    }

    //print last point
    plot.writeData();

    //write plot script
    plot.writeScript();

    //print stats
    std::cout << ResetConsole << "Simu stopped after " << GreenColor << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count() << "s" << ResetConsole << std::endl;
    std::cout << "\tTotal steps: " << GreenColor << steps << ResetConsole << std::endl;
    std::cout << "\tTotal: " << WhatColor << (loopTotalTime / 1000.0 / 1000.0) << "s" << ResetConsole << " - Mean: " << BoldCyanColor << ((loopTotalTime / steps) / 1000.0) << "ms" << ResetConsole << " - Min:" << GreenColor << (loopMinTime / 1000.0) << "ms" << ResetConsole << " - Max: " << RedColor << (loopMaxTime / 1000.0) << "ms" <<  ResetConsole << std::endl;
    //

    //end
    std::cout << ResetConsole;
    return EXIT_SUCCESS;
}

void printUsage() {
	std::cout << GreenColor << "Simulation" << ResetConsole << std::endl;
	std::cout << BoldCyanColor << "\tUsage: ./simulation <outfilename> " << ResetConsole << std::endl << std::endl;

}
